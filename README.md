# 6crickets ProgramPoster

The 6crickets ProgramPoster allows for automated posts to Facebook, Twitter and Instagram about any onlines classes on the 6crickets website.

## Initial Setup

1. Download and install Python 3.9.5 from https://www.python.org/downloads/.
2. Download and install Git from https://git-scm.com/downloads.
3. On the Bitbucket repository, click on the *Clone* button (located on the top right of the window). This will open a panel that says "Clone this repository".
4. Make sure that the dropdown on the top-right of the panel says *HTTPS*, then the entire command that begins with `git clone`.
5. Find or create a folder where you would like to download the repository, then open a terminal in that folder.
6. In the terminal, paste and run the copied command. This should download the project into your selected folder.
7. In the terminal, use *pip* to install the following Python packages: `requests` and `tweepy`. For more information on using *pip*, go to https://pip.pypa.io/en/stable/quickstart/.

## Config Setup

In order to use the Facebook, Twitter, Instagram and LinkedIn APIs, the accounts associated with your page on each site needs to be authenticated for its respective site. To do this, the program loads config and authentication data from the configuration file `data/config.json`. 

### **config_setup.py**

The script `config_setup.py` makes adding config info extremely simple! 
This script prompt you for all the information needed and automatically creates the config file. You can run this script by double-clicking it as long as Python is installed on your device.

However, you still need to do some work to gather the neccessary information, so read through the following instructions before running `config_setup.py`. 

`config_setup.py` and also be run through the console with the argument `-fb`, which will prompt the user for a new User Access Token, greatly simplifying the token renewal process.

### **Facebook and Instagram**

#### Initial Setup

1. Create a Facebook Page.
2. Create an Instagram Business Account.
3. Link your Facebook Page and Instagram Account. You can follow the steps [here](https://www.facebook.com/help/1148909221857370).

#### Client ID and Client Secret
In order to post to Facebook and Instagram automatically, we first need to create a developer application. Your application's **Client ID** and **Client Secret** (also known as the **App ID** and **App Secret**) are pieces of information about your application, and are needed for certain API calls. More information about these values can be found [here](https://developers.facebook.com/docs/development/create-an-app/app-dashboard/basic-settings#app-id).

1. Register as a developer on developers.facebook.com. You can follow the steps [here](https://developers.facebook.com/docs/development/register).
2. Once the developer account is set up, create a new app for the page. You can follow the steps [here](https://developers.facebook.com/docs/development/create-an-app).
3. In the app dashboard, go to Settings -> Basic, then locate the "App ID" and "App Secret" boxes. These are the app's **Client ID** and **Client Secret**, respectively.

#### User Access Token
A Facebook **User Access Token** in central is automating the config setup, as well as serving as a token that Instagram needs to publish content. While the most useful, it is also the most complex to acquire. More information about Access Tokens can be found [here](https://developers.facebook.com/docs/facebook-login/access-tokens/).

1. On the Facebook developer dashboard, locate the *Tools* button on the top bar. Hover over it, then click on *Graph API Explorer*. This will open the [Facebook Graph API Explorer](https://developers.facebook.com/docs/graph-api/explorer/).
2. In the Graph API Explorer, locate the *Access Token* panel on the right side of the screen. 
3. In the *Facebook App* dropdown, ensure that the selected app is correct.
4. In the *User of Page* dropdown, select *Get User Access Token*.
5. For *Permissions*, ensure that the `pages_show_list`, `pages_read_engagement`, `pages_manage_posts`, `instagram_basic` and `instagram_content_publish` permissions are all selected. You can read more about permissions [here](https://developers.facebook.com/docs/permissions/reference).
6. Click the *Generate Access Token* button near the top of the panel, then complete the [Facebook Login](https://developers.facebook.com/docs/facebook-login/overview) pop-up.
7. The long string of characters near the top of the panel is your **User Access Token**.

    **Note: This token will have to be renewed every 60 days.**

#### Page ID
A Page ID is needed for the program to know which Facebook Page to post to. If you want to learn more about Pages, see the [overview](https://developers.facebook.com/docs/pages/overview), [API reference](https://developers.facebook.com/docs/graph-api/reference/page/) or [publishing documentation](https://developers.facebook.com/docs/pages/publishing).
After the Client ID, Client Secret and User Access Token have all been entered, `config_setup.py` will automatically get the ID of your Facebook Page. If you have more than one Facebook Page, it will prompt you to pick the one that you want.

#### Page Access Token
The Page Access Token is a special token used to post to your Facebook Page. More information about Access Tokens can be found [here](https://developers.facebook.com/docs/facebook-login/access-tokens/).
After the Page ID is chosen, a permanent Page Access Token will be automatically generated, so no user input is needed. 

#### Instagram Business Account ID
Your Instagram Business Account ID is needed to know which Instagram account to post to. Since this is linked to your Page, this is collected automatically. You can read more about how it is used [here](https://developers.facebook.com/docs/instagram-api/guides/content-publishing).

### **Twitter**

Prerequisite: Have a Twitter account

#### API Key, API Key Secret, Access Token and Access Token Secret
All of these pieces of information are needed to access the API, but luckily they are easy to acquire. Read about the whole process on [the official Twitter API documentation page](https://developer.twitter.com/en/docs/twitter-api/getting-started/getting-access-to-the-twitter-api).

1. Register your Twitter account as a developer on developer.twitter.com. 
2. Create an app.
3. Upon completion of the app's creation, Twitter will show you the app's **API Key**, **API Key Secret**, **Access Token** and **Access Token Secret**.

### **LinkedIn**

**Note: The program poster does not post to LinkedIn, so config setup is not needed.**

Using the LinkedIn API also requires creating a developed app and getting an access token. To post Shares on LinkedIn, your organization's URN and an access token are needed, whose values need to be stored in `config.json` as `"in_org_urn"` and `"in_access_token"` respectively. Unfortunately, I will not be giving a step-by-step tutorial in acquiring those values. 

For more info regarding getting an access token, click [here](https://docs.microsoft.com/en-us/linkedin/shared/authentication/authorization-code-flow).

### **Bitly Config**

#### Access Token
Bitly's link shortening API is used when creating links to the 6crickets website, and needs an Access Token in order to use it.

1. Create a developer account on Bitly at https://dev.bitly.com/.
2. On the development dashboard, click your account dropdown (on the top right of the screen), then click *Profile Settings*.
3. In the profile settings side panel, click on *Generic Access Token** and follow the steps to create the **Access Token**.

### **Summary**

Once you know how to get all of these pieces of info, you can run `config_setup.py`.

## Automatically post about a 6crickets program

1. Find and open the modal of a program on the 6crickets website that you would like to post about.
2. Copy the URL. It should look something like: `https://www.6crickets.com/activities/online/classes-camps?min_start_date=YYYY-mm-dd&page=1&opened_modals=program-details&program_in_modal=00000#programs`.
3. Open the file `urls.txt` located in the `data` folder (if it doesn't exist, create it), then paste the URL in a new line. Any number of URLs can be put here, as long as they are on different lines. Once done, save and close the file.
4. Run `main.py`. This can be done by double-clicking it as long as Python is installed on your device. All programs that the URLs linked to will immediately have a post created and published to Facebook, Twitter and Instagram.

Links to past posts can be viewed in `data/past_posts.json`, with posts sorted by program id.

## Use as a Python package

The main class is the `smposter.SocialMediaPoster` class. 

```
from smposter import SocialMediaPoster

poster = SocialMediaPoster()
```

### Make a custom post and publish it

Publish a post with the `publish_to_facebook`, `publish_to_twitter`, `publish_to_instagram`, `publish_to_linkedin` or `publish_to_all` methods of the SocialMediaPoster class. These methods all accept two strings: the message, which is all the text in the post, and the URL of an image, which is the image to accompany the text. 

For example:
```
from smposter import SocialMediaPoster

poster = SocialMediaPoster()
message = "Enter you message here."
image_url = "www.example.com/image.jpg"

response = poster.publish_to_facebook(message, image_url)
print(response)
```

This will immediately attempt to create and publish a post on Facebook with the given message and image, and then return a Response object, which can be printed or stored.

## Updating

To update the ProgramPoster, open a terminal and navigate to the repository folder, then type `git pull`.