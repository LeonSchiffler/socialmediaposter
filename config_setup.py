from smposter.config import Config, FacebookConfig, InstagramConfig
from smposter import fb_api_util
import sys

def prompt_facebook_client_info():
    client_id = input("Please enter your App ID: ")
    if (len(client_id) > 0):
        Config.set_value("fb_client_id", client_id)
    client_secret = input("Please enter your App Secret: ")
    if (len(client_secret) > 0):
        Config.set_value("fb_client_secret", client_secret)

def prompt_facebook_user_access_token():
    sl_user_access_token = input("Please enter you User Access Token: ")
    if (len(sl_user_access_token) > 0):
        try:
            client_id = FacebookConfig.get_client_id()
            client_secret = FacebookConfig.get_client_secret()
        except KeyError:
            print("Unable to process User Access Token: Facebook App ID or Secret not set.")
            return
        ll_user_access_token = fb_api_util.exchange_user_access_token(client_id, client_secret, sl_user_access_token)
        Config.set_value("ig_user_access_token", ll_user_access_token)

def prompt_facebook_page_id():
    try:
        user_access_token = InstagramConfig.get_user_access_token()
    except KeyError:
        print("Unable to get Page ID: User Access Token not set.")
        return
    pages = fb_api_util.get_pages(user_access_token)
    if len(pages) < 1:
        print("Unable to get Page ID: No pages were found on your account.")
        return
    if len(pages) == 1:
        Config.set_value("fb_page_id", pages[0]["id"])
        return
    print("Your account owns the following accounts. Select the correct page (type the number).")

    for i in range(1,len(pages) + 1):
        page = pages[i]
        print("[{}] {}".format(i, page["name"]))
    
    valid_input = False
    while (not valid_input):
        num_str = input()
        try:
            num = int(num_str)
            if (num > 0 and num <= len(pages)):
                valid_input = True
            else:
                print("Invalid input: Number must be within the range.")
        except:
            print("Invalid input: Please enter a number.")
    Config.set_value("fb_page_id", pages[num]["id"])

def set_facebook_page_access_token():
    try:
        page_id = FacebookConfig.get_page_id()
        user_access_token = InstagramConfig.get_user_access_token()
    except KeyError:
        print("Unable to get Page Access Token: Page ID or User Access Token not set.")
        return
    ll_page_access_token = fb_api_util.get_ll_page_access_token(page_id, user_access_token)
    if (ll_page_access_token is not None):
        Config.set_value("fb_page_access_token", ll_page_access_token)
    else:
        print("Unable to get Page Access Token.")

def set_instagram_id():
    try:
        page_id = FacebookConfig.get_page_id()
        ll_page_access_token = FacebookConfig.get_page_access_token()
    except KeyError:
        print("Unable to get Instagram ID: Page ID or Page Access Token not set.")
        return
    ig_id = fb_api_util.get_instagram_id(page_id, ll_page_access_token)
    if (ig_id is not None):
        Config.set_value("ig_user_id", ig_id)
    else:
        print("Unable to get Instagram ID.")

def update_facebook_instagram_user_access_token():
    try:
        client_id = FacebookConfig.get_client_id()
        client_secret = FacebookConfig.get_client_secret()
    except KeyError:
        print("Unable to get User Access Token: Facebook App ID or Secret not set.")
        return
    ll_user_access_token = fb_api_util.get_ll_user_access_token(client_id, client_secret)
    if (ll_user_access_token is not None):
        Config.set_value("ig_user_access_token", ll_user_access_token)
        Config.save()
        print("User Access Token successfully updated.")
    else:
        print("Unable to update User Access Token.")

def setup_facebook_instagram():
    print("Starting Facebook and Instagram setup. Leave entries blank to keep the previous value.")
    prompt_facebook_client_info()
    prompt_facebook_user_access_token()
    prompt_facebook_page_id()
    print("Getting Page Access Token...")
    set_facebook_page_access_token()
    print("Getting Instagram Account ID...")
    set_instagram_id()
    print("Facebook and Instagram setup finished.")
    Config.save()

def setup_twitter():
    print("Starting Twitter setup. Leave entries blank to keep the previous value.")
    api_key = input("Please enter your API Key: ")
    if (len(api_key) > 0):
        Config.set_value("twt_consumer_key", api_key)
    api_secret = input("Please enter your API Key Secret: ")
    if (len(api_secret) > 0):
        Config.set_value("twt_consumer_secret", api_secret)
    access_token = input("Please enter your Access Token: ")
    if (len(access_token) > 0):
        Config.set_value("twt_access_token", access_token)
    access_token_secret = input("Please enter your Access Token Secret: ")
    if (len(access_token_secret) > 0):
        Config.set_value("twt_access_token_secret", access_token_secret)
    print("Twitter setup finished.")
    Config.save()

def setup_bitly():
    print("Starting Bitly setup. Leave entries blank to keep the previous value.")
    access_token = input("Please enter your Access Token: ")
    if (len(access_token) > 0):
        Config.set_value("bitly_access_token", access_token)
    print("Bitly setup finished.")
    Config.save()

def print_help():
    print("Help for config_setup.py:")
    print("config_setup.py is a script to help with setting up configuration of the SocialMediaPoster.")
    print("This script can be used with up to one argument:")
    print(" - No Arguments: Enter in new config values for all platforms. This will replace any old values.")
    print(" - '-fb': create a new Facebook/Instagram user access token.")
    print(" - '-h' or '-help': Provides help.")

def yes_no_response(prompt: str) -> bool:
    "Prompt the user for a Y or N response. Returns the result. Keeps asking until a valid response is recieved."
    positive_responses = ["y", "Y"]
    negative_responses = ["n", "N"]

    response = input("{} [Y/N]: ".format(prompt))
    valid_response = False
    accept = False
    while not valid_response:
        if (response in positive_responses):
            valid_response = True
            accept = True
        elif (response in negative_responses):
            valid_response = True
            accept = False
        else:
            response = input("Please enter a valid response. (Either 'Y' or 'N'): ")
    return accept

if (__name__ == "__main__"):
    if (len(sys.argv) > 1):
        # At least 1 argument
        if (sys.argv[1] == "-h" or sys.argv[1] == "-help"):
            # Help with "-h" or "-help"
            print_help()
        elif (sys.argv[1] == "-fb"):
            # Update
            if yes_no_response("Do you want to update the user access token for Facebook and Instagram?"):
                prompt_facebook_user_access_token()
        else:
            print("Unrecognized arguments. Use '-h' or '-help' to see available arguments.")
    else:
        # No arguments - new setup
        if yes_no_response("Do you want to set up configuration for Facebook and Instagram?"):
            setup_facebook_instagram()
        if yes_no_response("Do you want to set up configuration for Twitter?"):
            setup_twitter()
        if yes_no_response("Do you want to set up configuration for Bitly?"):
            setup_bitly()
        print("Config Setup finished.")