from smposter.core import SocialMediaPoster
from smposter.posts import Response
from programposter.program_post import ParentReview, ProgramPost
from datetime import datetime
import json
import requests
import re

input_filename = "data/urls.txt"
output_filename = "data/past_posts.json"

def main():
    "Creates and publishes social media posts for each programs specified in data/urls.txt."
    # Read urls.txt and get all programs
    programs = read_input()

    # Post all programs
    poster = SocialMediaPoster()
    for program in programs:
        print("Creating next program post.")
        post = create_program_post(program)
        fb_response = poster.publish_to_facebook(post.format_message_for_facebook(), post.image_url)
        twt_response = poster.publish_to_twitter(post.format_message_for_twitter(), post.image_url)
        ig_response = poster.publish_to_instagram(post.format_message_for_instagram(), post.image_url)
        write_to_output(post.program_id, fb_response, twt_response, ig_response)

def read_input() -> list[dict]:
    "Returns a list of all programs from the input URLS from data/urls.txt."
    # Read all URLs
    urls = []
    with open(input_filename, "r+") as input:
        for line in input:
            urls.append(line)
        # clear
        input.truncate(0)
    
    # Get program info for each URL
    programs = []
    for url in urls:
        # Use RegEx to find program id in URL
        # RegEx pattern: matches digits exactly between "program_in_modal=" and one of either "&" or "#"
        id_match = re.search(r"(?<=program_in_modal=)(\d+)(?=[&#])", url)
        if id_match:
            id = id_match.group()
            program = get_program(id)
            programs.append(program)
    return programs

def get_program(program_id:str) -> dict:
    "Returns the program with the given ID."
    url = "https://www.6crickets.com/api/session" \
        + "?program={}".format(program_id)
    response = requests.get(url)
    response_json = response.json()
    program = response_json["results"][0]
    return program

def get_program_page() -> list[dict]:
    "Gets a list of 6crickets programs."
    url = "https://www.6crickets.com/api/session" \
        + "?min_start_date={}".format(get_date_str()) \
        + "&online=&page=1" \
        + "#programs"
    response = requests.get(url)
    programs = response.json()["results"]
    return programs

def create_program_post(program) -> ProgramPost:
    "Create a program post for a program."
    # Simple Info
    name = program["name"]
    program_id = program["id"]
    provider_name = program["provider_name"]
    tagline = program["social_media_tag"]
    description = program["summary"]

    # Image URL
    if program["image_url"]:
        image_url = program["image_url"]
    elif program["youtube_url"]:
        # If no image, we need to get thumbnail from youtube
        youtube_url = program["youtube_url"]
        video_id = youtube_url.split("/")[-1]
        image_url = "https://img.youtube.com/vi/{}/0.jpg".format(video_id)
    else:
        image_url = None

    # Registration URL
    register_url = "https://www.6crickets.com/activities/online/classes-camps" \
                 + "?opened_modals=program-details&program_in_modal={}".format(program_id) \
                 + "#programs"

    # Parent Review
    parent_review = get_parent_review(program_id)

    # Provider info
    provider_id = program["provider_id"]
    provider_name = program["provider_name"]
    provider_url = get_provider_url(provider_id)

    # TODO: Hashtags
    
    return ProgramPost(program_id = program_id, program_name = name, 
                       register_url = register_url, tagline = tagline, image_url = image_url, 
                       provider_name = provider_name, provider_url =  provider_url, 
                       description = description, parent_review = parent_review, hashtags = [])

def get_parent_review(program_id) -> ParentReview:
    "Get a parent review for a program."
    # Uses 6crickets API session_reviews endpoint
    url = "https://www.6crickets.com/api/session_reviews?program={}".format(program_id)
    response = requests.get(url)
    reviews = response.json()
    if (len(reviews) > 0):
        for review_obj in reviews:
            review = review_obj["review"]
            rating = review["rating_overall"]
            text = review["comments"]
            # Rating needs to be above minimum, and text between a certain length
            if text:
                if (rating >= ParentReview.min_rating and len(text) > ParentReview.min_len and len(text) < ParentReview.max_len):
                    parent_review = ParentReview(rating, text)
                    return parent_review
    return None

def get_provider_url(provider_id) -> str:
    "Get a provider's website URL."
    # Uses 6crickets API provider endpoint
    url = "https://www.6crickets.com/api/provider?id={}".format(provider_id)
    response = requests.get(url)
    provider = response.json()
    return provider[0]["website"]

def write_to_output(program_id:str, fb_response:Response, twt_response:Response, ig_response:Response):
    response_obj = {
        "date": get_date_str(),
    }
    if fb_response:
        response_obj["facebook"] = {
            "status": fb_response.response_text,
            "id": fb_response.post_id,
            "url": fb_response.post_url
        }
    if twt_response:
        response_obj["twitter"] = {
            "status": fb_response.response_text,
            "id": twt_response.post_id,
            "url": twt_response.post_url
        }
    if ig_response:
        response_obj["instagram"] = {
            "status": fb_response.response_text,
            "id": ig_response.post_id,
            "url": ig_response.post_url
        }

    # Load output file
    with open(output_filename, "r") as output:
        try:
            output_json = json.load(output)
        except:
            output_json = {}

    # Append data to output
    if program_id not in output_json:
        output_json[program_id] = []
    output_json[program_id].append(response_obj)

    # Overwrite output file
    with open(output_filename, "w") as output:
        json.dump(output_json, output, indent=4)

def get_date_str():
    "Get today's date in the format: 'YYYY-mm-dd'."
    return datetime.now().strftime("%Y-%m-%d")