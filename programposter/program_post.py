from smposter.config import BitlyConfig
import requests

class ParentReview:
    min_rating = 4
    min_len = 20
    max_len = 200

    def __init__(self, score:int, text:str):
        if (score > 5):
            score = 5
        if (score < 1):
            score = 1
        self.score = score
        self.text = text

class ProgramPost:

    

    def __init__(self, program_id:str, program_name:str, register_url:str, tagline:str,
                 image_url:str = "", provider_name:str = "", provider_url:str = "",  
                 description:str = "", parent_review:ParentReview = None, hashtags:list[str] = []):
        self.program_id = program_id
        self.program_name = program_name
        self.register_url = register_url
        self.tagline = tagline
        self.provider_name = provider_name
        self.provider_url = provider_url
        self.image_url = image_url
        self.hashtags = ["6crickets", "enrichment", "childledlearning", "learningisfun"]
        self.hashtags.extend(hashtags)
        self.description = description
        if (parent_review):
            self.parent_review = parent_review

    def construct_message(self, max_len:int) -> str:
        # Online Program: {name}
        # {tagline} + Register at {bit.ly/link}.
        # {description} + {parent_review}
        # {provider_name}[provider_url]
        # [#{hastags}]

        # Default: name + tagline + register_link
        name_text = "Online Program: {}\n\n".format(self.program_name)
        body_text = "{}\nRegister at: {}\n\n".format(self.tagline, self.shorten_register_url())
        provider_text = "{}: {}\n\n".format(self.provider_name, self.provider_url)
        parent_review_text = "{}\n\n".format(self.construct_parent_review())

        base_len = len(name_text) + len(body_text) + len(provider_text)

        # Try add description
        description_text = "{}\n\n".format(self.description)
        if (base_len + len(description_text) < max_len):
            body_text = body_text + description_text
            base_len += len(description_text)
        
        # Try add parent review
        if (base_len + len(parent_review_text) < max_len):
            body_text = body_text + parent_review_text
            base_len += len(parent_review_text)

        message = "{}{}{}".format(name_text, body_text, provider_text)

        # Try add hashtags
        for tag in self.hashtags:
            hashtag = "#{} ".format(tag)
            if (len(message) + len(hashtag) > max_len):
                break
            message += hashtag

        return message

    def construct_parent_review(self) -> str:
        stars = ""
        for i in range(self.parent_review.score):
            # Add filled star
            stars += "\u2605"
        for i in range(5 - self.parent_review.score):
            # Add empty star
            stars += "\u2606"
        return "Parent Review:\n{}\n\"{}\"".format(stars, self.parent_review.text)

    def shorten_register_url(self) -> str:
        # Use bitly API to shorten the register link
        url = "https://api-ssl.bitly.com/v4/shorten"
        headers = {
            "Authorization": "Bearer {}".format(BitlyConfig.get_access_token()),
            'Content-Type': 'application/json',
        }
        data = {
            # "group_guid": bitly_guid,
            "domain": "bit.ly",
            "long_url": self.register_url
        }
        response = requests.post(url, headers=headers, json=data)
        return response.json()["link"]

    def format_message_for_facebook(self):
        # no restrictions
        max_len = 10000
        return self.construct_message(max_len)

    def format_message_for_twitter(self):
        # text limit = 280 characters
        max_len = 280
        return self.construct_message(max_len)

    def format_message_for_instagram(self):
        # caption limit = 2200
        # hashtag limit = 30
        # image extension = jpg
        # image width = 320 to 1080 px
        # image aspect ratio = 1.91:1 to 4:5
        max_len = 2200
        return self.construct_message(max_len)