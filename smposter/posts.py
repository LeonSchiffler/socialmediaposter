from smposter.config import FacebookConfig, LinkedInConfig, TwitterConfig, InstagramConfig
from abc import ABC, abstractmethod
import requests
import tweepy
import os

class Response:
    
    def __init__(self, platform:str, successful:bool, response_text:str, 
                 message:str = "", image_url:str = "", 
                 post_id:str = "", post_url:str = ""):
        self.platform = platform
        self.message = message
        self.image_url = image_url
        self.successful = successful
        self.response_text = response_text
        self.post_id = post_id
        self.post_url = post_url

    def json(self):
        json = {
            "platform": self.platform,
            "message": self.message,
            "image_url": self.image_url,
            "successful": self.successful,
            "text": self.response_text,
            "id": self.post_id,
            "url": self.post_url,
        }
        return json

    def __str__(self):
        return str(self.json())
    
    def __repr__(self):
        return str(self)

class Post(ABC):
    platform = ""

    def __init__(self, message:str, image_url:str):
        self.message = message
        self.image_url = image_url

    def __str__(self) -> str:
        string = "platform: " + self.platform + ", message: " + self.message + ", image_url: " + self.image_url
        return "{" + string + "}"

    def __repr__(self):
        return str(self)

    @abstractmethod
    def publish(self) -> Response:
        pass

class FacebookPost(Post):
    platform = "facebook"

    def publish(self) -> Response:
        "Post and publish on Facebook."
        if self.image_url:
            # API: https://developers.facebook.com/docs/graph-api/reference/page/photos/#Creating
            url = "https://graph.facebook.com/{}/photos".format(FacebookConfig.get_page_id())
            payload = {
                "url": self.image_url,
                "caption": self.message
            }
        else:
            # API: https://developers.facebook.com/docs/graph-api/reference/v10.0/page/feed#publish
            url = "https://graph.facebook.com/{}/feed".format(FacebookConfig.get_page_id())
            payload = {
                "message": self.message
            }
        payload["access_token"] = FacebookConfig.get_page_access_token()
        response = requests.post(url = url, data = payload)
        return self.__create_response(response)

    def __create_response(self, response:requests.Response) -> Response:
        platform = self.platform
        status_code = response.status_code
        if (status_code == 200):
            text = "Post Successful."
            id = str(response.json()["id"])
            url = "https://www.facebook.com/" + id
            return Response(platform, True, text, self.message, self.image_url, id, url)
        else:
            text = response.text
            return Response(platform, False, text, self.message, self.image_url)

class TwitterPost(Post):
    platform = "twitter"

    def publish(self) -> Response:
        "Post and publish on Twitter."
        # API: https://docs.tweepy.org/en/latest/api.html#post-retrieve-and-engage-with-tweets
        auth = tweepy.OAuthHandler(TwitterConfig.get_consumer_key(), TwitterConfig.get_consumer_secret())
        auth.set_access_token(TwitterConfig.get_access_token(), TwitterConfig.get_access_token_secret())
        api = tweepy.API(auth)
        media_ids = []
        if (self.image_url):
            # Download the image and save locally
            dl_response = requests.get(self.image_url)
            if (dl_response.status_code == 200):
                image_filename = "data/" + self.image_url.split("/")[-1]
                with open(image_filename, "wb") as image_file:
                    image_file.write(dl_response.content)
                # Upload image to Twitter
                try:
                    media_response = api.media_upload(image_filename)
                    media_ids.append(media_response.media_id)
                finally:
                    os.remove(image_filename)
        response = api.update_status(status = self.message, media_ids = media_ids)
        return self.__create_response(response)

    def __create_response(self, response):
        platform = self.platform
        text = "Post Successful."
        id = response.id_str
        url = "https://twitter.com/i/web/status/{}".format(id)
        return Response(platform, True, text, self.message, self.image_url, id, url)

class InstagramPost(Post):
    platform = "instagram"

    def publish(self) -> Response:
        "Post and publish on Instagram."
        # API docs: https://developers.facebook.com/docs/instagram-api/guides/content-publishing#publish-photos
        media_url = "https://graph.facebook.com/{}/media".format(InstagramConfig.get_user_id())
        media_data = {
            "image_url": self.image_url,
            "access_token": InstagramConfig.get_user_access_token()
        }
        if self.message:
            media_data["caption"] = self.message
        media_response = requests.post(url = media_url, data = media_data)
        if (media_response.status_code != 200):
            return self.__create_response(media_response)

        publish_url = "https://graph.facebook.com/{}/media_publish".format(InstagramConfig.get_user_id())
        publish_data = {
            "creation_id": media_response.json()["id"],
            "access_token": InstagramConfig.get_user_access_token()
        }
        publish_response = requests.post(url = publish_url, data = publish_data)
        return self.__create_response(publish_response)

    def __create_response(self, response:requests.Response) -> Response:
        platform = self.platform
        status_code = response.status_code
        if (status_code == 200):
            text = "Post Successful."
            id = str(response.json()["id"])
            url = self.__get_post_permalink(id)
            return Response(platform, True, text, self.message, self.image_url, id, url)
        else:
            text = response.text
            return Response(platform, False, text, self.message, self.image_url)

    def __get_post_permalink(self, post_id:str) -> str:
        url = "https://graph.facebook.com/v10.0/{}".format(post_id)
        params = {
            "fields": ["permalink"],
            "access_token": InstagramConfig.get_user_access_token()
        }
        response = requests.get(url, params = params)
        if (response.status_code == 200):
            return response.json()["permalink"]
        return ""

class LinkedInPost(Post):
    platform = "linkedin"

    def publish(self):
        "Publishes the post on LinkedIn."
        url = "https://api.linkedin.com/v2/shares"
        headers = {
            "Authorization": "Bearer {}".format(LinkedInConfig.get_access_token())
        }
        payload = {
            "distribution": {
                "linkedInDistributionTarget": {}
            },
            "owner": "urn:li:organization:{}".format(LinkedInConfig.get_org_urn()),
            # "subject": "subject",
            "text": {
                "text": self.message
            }
        }
        if (self.image_url):
            payload["content"] = self.attach_image(self)
        response = requests.post(url, json = payload, headers = headers)
        return self.__create_response(response)

    def attach_image(self) -> dict:
        "Register and upload the image. Returns the content info."
        upload_url, asset_id = self.register_media()
        self.upload_media(upload_url)

        content_info = {
            "contentEntities": [
                {
                    "entity": "{}".format(asset_id)
                }
            ],
            # "description": "content description",
            # "title": "content title",
            "shareMediaCategory": "IMAGE"
        }
        return content_info  

    def register_media(self):
        "Registers media for upload. Returns the upload URL and asset ID."
        url = "https://api.linkedin.com/v2/assets?action=registerUpload"
        payload = {
            "registerUploadRequest":{
                "owner":"urn:li:organization:{}".format(LinkedInConfig.get_org_urn()),
                "recipes":[
                    "urn:li:digitalmediaRecipe:feedshare-image"
                ],
                "serviceRelationships":[
                    {
                        "identifier":"urn:li:userGeneratedContent",
                        "relationshipType":"OWNER"
                    }
                ],
                "supportedUploadMechanism":[
                    "SYNCHRONOUS_UPLOAD"
                ]
            }
        }
        
        headers = {
            "Authorization": "Bearer {}".format(LinkedInConfig.get_access_token()),
            "Content-Type": "application/json"
        }
        response = requests.post(url, json = payload, headers = headers)
        response_json = response.json()
        upload_url = response_json["value"]["uploadMechanism"]["com.linkedin.digitalmedia.uploading.MediaUploadHttpRequest"]["uploadUrl"]
        asset_id = response_json["value"]["asset"]
        return upload_url, asset_id

    def upload_media(self, upload_url):
        "Uploads the image to the upload url."
        # Download the image and save locally
        dl_response = requests.get(self.image_url)
        assert(dl_response.status_code == 200), "Invalid image url."
        headers = {
            "Authorization": "Bearer {}".format(LinkedInConfig.get_access_token())
        }
        response = requests.put(upload_url, data = dl_response.content, headers = headers)
        assert(response.status_code == 201), "LinkedIn media upload failed."

    def __create_response(self, response):
        platform = self.platform
        status_code = response.status_code
        if (status_code == 200):
            text = "Post Successful."
            id = str(response.json()["id"])
            # No URL for LinkedIn shares
            return Response(platform, True, text, self.message, self.image_url, id)
        else:
            text = response.text
            return Response(platform, False, text, self.message, self.image_url)