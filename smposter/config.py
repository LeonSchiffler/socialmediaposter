import json
import requests.utils

class Config:
    config:dict = None
    __config_filename = "data/config.json"

    @staticmethod
    def load():
        "Loads the all config data into memory from the config file."
        with open(Config.__config_filename, "r") as config_file:
            try:
                Config.config = json.load(config_file)
            except:
                Config.config = {}

    @staticmethod
    def save():
        "Saves the config data into the config file."
        with open(Config.__config_filename, "w") as config_file:
            json.dump(Config.config, config_file, indent=4)

    @staticmethod
    def set_value(key:str, value):
        "Sets the config key to the specified value."
        Config.config[key] = value

class FacebookConfig:

    @staticmethod
    def get_client_id() -> str:
        "Returns the Facebook app client id."
        return Config.config["fb_client_id"]

    @staticmethod
    def get_client_secret() -> str:
        "Returns the Facebook app client secret."
        return Config.config["fb_client_secret"]

    @staticmethod
    def get_page_id() -> str:
        "Returns the Facebook page id."
        return Config.config["fb_page_id"]

    @staticmethod
    def get_page_access_token() -> str:
        "Returns the Facebook page access token."
        return Config.config["fb_page_access_token"]

class TwitterConfig:

    # Use requests.utils.quote to format keys to be URL-safe
    @staticmethod
    def get_consumer_key() -> str:
        "Returns the Twitter consumer key."
        return requests.utils.quote(Config.config["twt_consumer_key"], safe="")
        
    @staticmethod
    def get_consumer_secret() -> str:
        "Returns the Twitter consumer secret."
        return requests.utils.quote(Config.config["twt_consumer_secret"], safe="")

    @staticmethod
    def get_access_token() -> str:
        "Returns the Twitter access token."
        return requests.utils.quote(Config.config["twt_access_token"], safe="")

    @staticmethod
    def get_access_token_secret() -> str:
        "Returns the Twitter access token secret."
        return requests.utils.quote(Config.config["twt_access_token_secret"], safe="")

class InstagramConfig:
    @staticmethod
    def get_user_access_token() -> str:
        "Returns the Instagram user token."
        return Config.config["ig_user_access_token"]

    @staticmethod
    def get_user_id() -> str:
        "Returns the Instagram user id."
        return Config.config["ig_user_id"]

class LinkedInConfig:
    @staticmethod
    def get_org_urn() -> str:
        "Returns the LinkedIn organization URN."
        return Config.config["in_org_urn"]

    @staticmethod
    def get_access_token() -> str:
        "Returns the LinkedIn access token."
        return Config.config["in_access_token"]

class BitlyConfig:
    @staticmethod
    def get_access_token() -> str:
        "Returns the Bitly access token."
        return Config.config["bitly_access_token"]