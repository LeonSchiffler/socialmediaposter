from smposter.posts import Response, Post, FacebookPost, TwitterPost, InstagramPost, LinkedInPost

class SocialMediaPoster:

    def __publish(self, post: Post) -> Response:
        try:
            print("Publishing to {}...".format(post.platform))
            response = post.publish()
            print(response.response_text)
        except Exception as e:
            response = Response(post.platform, False, str(e.args[0]), post.message, post.image_url)
        return response

    def publish_to_facebook(self, message:str = "", image_url:str = "") -> Response:
        "Create and publish a Facebook post with the message and image URL."
        post = FacebookPost(message, image_url)
        return self.__publish(post)
        
    def publish_to_twitter(self, message:str = "", image_url:str = "") -> Response:
        "Create and publish a Twitter post with the message and image URL."
        post = TwitterPost(message, image_url)
        return self.__publish(post)

    def publish_to_instagram(self, message:str = "", image_url:str = "") -> Response:
        "Create and publish an Instagram post with the message and image URL."
        post = InstagramPost(message, image_url)
        return self.__publish(post)

    def publish_to_linkedin(self, message:str = "", image_url:str = "") -> Response:
        "Create and publish a LinkedIn post with the messag and image URL."
        post = LinkedInPost(message, image_url)
        return self.__publish(post)

    def publish_to_all(self, message:str = "", image_url:str = "") -> list[Response]:
        "Create and publish a post to Facebook, Twitter, Instagram and LinkedIn with the message and image_url."
        responses = []
        responses.append(self.publish_to_facebook(message, image_url))
        responses.append(self.publish_to_twitter(message, image_url))
        responses.append(self.publish_to_instagram(message, image_url))
        responses.append(self.publish_to_linkedin(message, image_url))
        return responses