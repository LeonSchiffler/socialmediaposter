from http.server import HTTPServer, BaseHTTPRequestHandler
import requests
import requests.utils
import webbrowser

port = 8080
redirect_url = requests.utils.quote("https://localhost:{}/".format(port), safe = "")

class HTTPServerHandler(BaseHTTPRequestHandler):
    "HTTP Server callbacks to handle Facebook OAuth redirects."
    # FB OAuth login flow from: https://www.pmg.com/blog/logging-facebook-oauth2-via-command-line-using-python/

    def __init__(self, request, address, server, client_id, client_secret):
        self.client_id = client_id
        self.client_secret = client_secret
        super().__init__(request, address, server)

    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()

        print(self.path)

        if 'code' in self.path:
            self.auth_code = self.path.split('=')[1]
            access_token = self.exchange_code_for_access_token(self.auth_code)
        else:
            access_token = None
        setattr(self.server, "access_token", access_token)
        self.wfile.write(bytes("<html><h1>You may now close this window.</h1></html>", "utf-8"))

    # Disable logging from the HTTP Server
    def log_message(self, format, *args):
        return

    def exchange_code_for_access_token(self, code: str) -> str:
        "Exchanges a login code for an access token."
        url = ("https://graph.facebook.com/v10.0/oauth/access_token?"
            + "client_id=" + self.client_id
            + "&redirect_uri=" + redirect_url
            + "&client_secret=" + self.client_secret
            + "&code=" + code)
        response = requests.get(url)
        if "access_token" in response.json():
            token = response.json()["access_token"]
            return token
        return None

def get_ll_user_access_token(client_id: str, client_secret: str) -> str:
    "Logs in to facebook and returns the access token."
    # NOTE: Does not work at the moment, server receives bad request for some reason
    # Official docs: https://developers.facebook.com/docs/facebook-login/manually-build-a-login-flow

    url = ("https://www.facebook.com/dialog/oauth?"
        + "client_id=" + client_id
        + "&redirect_uri=" + redirect_url
        + "&state="
        + "&scope=pages_show_list,pages_read_engagement,pages_manage_posts,instagram_basic,instagram_content_publish")

    # Create HTTPServer to handle redirect
    httpServer = HTTPServer(("localhost", port), 
        lambda request, address, server: HTTPServerHandler(request, address, server, client_id, client_secret))

    # Secure with Self-Signed Certificate
    # Generate new cert with (bash):
    # $ openssl req -x509 -newkey rsa:2048 -new -out localhost.pem -keyout localhost.pem -days 3650 -nodes
    # httpServer.socket = ssl.wrap_socket(httpServer.socket, 
    #                                     certfile = "localhost.pem",
    #                                     server_side = True,
    #                                     ssl_version = ssl.PROTOCOL_TLS)

    # Open browser to log in
    webbrowser.open_new(url)

    # Handle redirect request from the browser
    httpServer.timeout = 120
    httpServer.handle_request()

    # Exchange for long-lived token and return
    sl_user_access_token = httpServer.access_token
    try:
        ll_user_access_token = exchange_user_access_token(client_id, client_secret, sl_user_access_token)
        return ll_user_access_token
    except:
        return None

def get_access_token(url: str, payload) -> str:
    "Get the access token for the given url and payload."
    response = requests.get(url, params = payload)
    if (response.status_code == 200):
        return response.json()["access_token"]
    return None

def get_ll_page_access_token(page_id: str, ll_user_access_token: str) -> str:
    "Get long-lived page access token using a long-lived user token."
    # API: https://developers.facebook.com/docs/pages/access-tokens#get-a-page-access-token
    url = "https://graph.facebook.com/{}".format(page_id)
    payload = { 
        "fields": "access_token",
        "access_token": ll_user_access_token
    }
    return get_access_token(url, payload)

def exchange_user_access_token(client_id: str, client_secret: str, sl_user_access_token: str) -> str:
    "Exchange short-lived user token for long-lived user token."
    # API: https://developers.facebook.com/docs/graph-api/using-graph-api/common-scenarios#convert-to-a-long-lived-token
    url = "https://graph.facebook.com/oauth/access_token"
    payload = {
        "grant_type": 'fb_exchange_token',
        "client_id": client_id,
        "client_secret": client_secret,
        "fb_exchange_token": sl_user_access_token,
        "access_token": sl_user_access_token
    }
    return get_access_token(url, payload)

def get_pages(user_access_token: str):
    "Return the names and IDs of all accounts owned by a user."
    url = ("https://graph.facebook.com/me?fields=accounts"
        + "&access_token=" + user_access_token)
    response = requests.get(url)
    pages = []
    if (response.status_code == 200):
        response_json = response.json()
        accounts = response_json["accounts"]["data"]
        for account in accounts:
            page = {
                "name": account["name"],
                "id": account["id"]
            }
            pages.append(page)
        pass
    return pages

def get_instagram_id(page_id: str, page_access_token: str):
    "Returns the ID of the Instagram Business Account connected to a page."
    url = ("https://graph.facebook.com/" + page_id
        + "?fields=instagram_business_account"
        + "&access_token=" + page_access_token)
    response = requests.get(url)
    if (response.status_code == 200):
        return response.json()["instagram_business_account"]["id"]
    return None